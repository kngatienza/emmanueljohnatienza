<!DOCTYPE html >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> -->
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script> -->

    <!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light growthops-header">
        <img src="../Growthops-Jr-Web-Developer-Exam/assets/logo.png" class="ml-1rm" width="50px">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse pl-6rm" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link font-white" href="#">OUR PRODUCTS</a>
            </li>
            <li class="nav-item">
              <a class="nav-link font-white" href="#">ABOUT US</a>
            </li>
            <li class="nav-item">
              <a class="nav-link font-white" href="#">LIVE BETTER</a>
            </li>
            <li class="nav-item">
                <a class="nav-link font-white" href="#">CLAIMS & SUPPORT</a>
            </li>
            <li class="nav-item">
                <a class="nav-link font-white" href="#">MY ACCOUNT</a>
            </li>
            <!--<li class="nav-item">
              <a class="nav-link disabled" href="#">Disabled</a>
            </li>-->
          </ul>
        </div>
    </nav>
    <div class="module-header">
        <h1 align="center"><b>DISTRICT MANAGER</b></h1>
    </div>
    <div style="padding-left: 8rem; padding-right: 8rem" class="pt-50 media-padding">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <p>Filter By Age</p>
                <div class="form-group">
                    <select class="form-control" id="filterAge">
                        <option value="All">All</option>
                        <option value="20_and_below">20 and below</option>
                        <option value="21_to_39">21 to 39</option>
                        <option value="40_and_above">40 and above</option>
                    </select>
                </div>
            </div>
        </div>
        <hr class="hr1">
        <div class="mb-2rm">
            <div class="row pt-1rm">
            <?php 
                $url = 'http://www.mocky.io/v2/5d73bf3d3300003733081869?fbclid=IwAR3S94_T2dl35cZlfx2UmRt-c0DVpfF-kg9BIubXnGIEYLff-84zehGFhdM';
                $json_data = file_get_contents($url);
                $data = json_decode($json_data);
                // print_r($data);
            ?>
            <?php
                foreach($data as $value){
                
            ?>
                <div class="media-col4 col-md-4 media-card">
                    <div class="card">
                        <div class="card-body bg-card media-center">
                            <div class="row">
                                <div class="col-md-3 col-sm-3">
                                    <img src="../Growthops-Jr-Web-Developer-Exam/assets/userphoto.png" width="80px">
                                </div>
                                <div class="media-col-card9 col-md-9 col-sm-9">
                                    <p class="personal-name mb-0rm"><?php echo $value->name; ?></p>
                                    <span>Email: <b><?php echo $value->email; ?></b></span><br>
                                    <span>Mobile: <b><?php echo $value->phone; ?></b></span><br>
                                    <span>Company: <b><?php echo $value->company; ?></b></span><br>
                                    <span>Address: <b><?php echo $value->address->street; ?> </b></span> <br>
                                    <span>Website <b><?php echo $value->website; ?> </b></span><br>
                                    <span>Age: <b><?php echo $value->age; ?></b></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                    }
                ?>
            </div>
        </div>
        <hr class="hr1">
        <div class="mb-3rm row">
            <div class="col-md-6">
                <span>Date</span><br>
                <div>
                    <div class="row">
                        <div class="col-sm-6">
                            <input placeholder="DD/MM/YYYY" id="datepicker"/>
                        </div>
                        <div class="col media-btn-convert" style="padding-left: 0rem">
                            <button type="button" id="formatDate" class="formatDate btn btn-convert">CONVERT</button>
                        </div>
                    </div>
                </div>
            </div>
                
        </div>    
        
    </div>


<script type="text/javascript">
    $('#datepicker').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'dd-mm-yyyy'
    });

    $('#formatDate').on('click', function () {
        var date = $('#datepicker').val();
        var formattedDate = date;
        alert(date);
    });
    
    // $('#formatDate').on('click', function () {
    //     var date = $('#datepicker').val();
    //     console.log('this returns '+date)
    // });

    // $('#formatDate').on('click', function () {
    //     var date = $('#datepicker').val();
    //     $new_date = date("yyyy-mm-dd", date);
    //     alert(date);
    // });

    // function ageFilter(){
    //     if(document.getElementById('filterAge').value=="20_and_below"){
            
    //     }
    // }

    
    function countMe(){
        for ( var x = 1; x <= 100; x++ )
        {
            if ( x%3 === 0 )
            {
                console.log( "Foo" );
            }
            else if ( x%5 === 0 ) 
            {
                console.log( "Bar" );
            }
            else
            {
                console.log(x);
            }
        }
    }
    console.log(countMe());
    
        
</script>

</body>
</html>

<style>
    .font-white{
        color: white!important;
    }
    .growthops-header{
        background-color: #d21045;
    }
    .pl-1rm{
        padding-left: 1rem;
    }
    .pl-6rm{
        padding-left: 6rem;
    }
    .p1rm{
        padding: 1rem;
    }
    .pt-1rm{
        padding-top: 1rem;
    }
    .pt-50{
        padding-top: 50px;
    }
    .ml-1rm{
        margin-left: 1rem;
    }
    .mb-0rm{
        margin-bottom: 0rem;
    }
    .mb-1rm{
        margin-bottom: 1rem;
    }
    .mb-2rm{
        margin-bottom: 2rem;
    }
    .mb-3rm{
        margin-bottom: 3rem;
    }
    .nav-link{
        transition: .2s;
    }
    .nav-link:hover{
        transform: scale(1.1);
        color: #f0f0f0!important;
    }
    .module-header{
        padding: 1rem;
        background: #fbfbfb;
    }
    .hr1{
        height: 2px!important;
        background: #707070;
        margin-top: 1.5rem;
        color: #707070;
    }
    .personal-name{
        font-size: 1.8rem;
        font-weight: 700;
    }
    .btn-convert{
        background: #d21045;
        color: #fff;
    }
    .btn-convert:hover{
        background: #fff;
        color: #d21045;
        border-color: #d21045;
    }
    .bg-card {
        /* background: #FBFBFB; */
        border: none;
        box-shadow: 0px 2px 10px #f0f0f0;
    }

/* Extra small devices (portrait phones, less than 576px) */
@media (max-width: 575px){
    .navbar-collapse{
       text-align: center;
       padding-left: 0rem;
       padding-right: 0rem;
   }
   .media-btn-convert{
       text-align: center;
       padding-top: 1rem;
   }
   .media-padding{
       padding-left: 1rem!important;
       padding-right: 1rem!important;
   }
   .media-card{
       padding-top: 1rem;
   }
   .media-center{
       text-align: center;
   }
}

/* Small devices (landscape phones, 576px and up) */
@media (min-width: 576px) and (max-width: 767px) {  
    .navbar-collapse{
       text-align: center;
       padding-left: 0rem;
       padding-right: 0rem;
   }
   .media-card{
       padding-top: 1rem;
   }
   .media-padding{
       padding-left: 1rem!important;
       padding-right: 1rem!important;
   }
}
/* Medium devices (tablets, 768px and up) */
@media (min-width: 768px) and (max-width: 991px) {  
   .navbar-collapse{
       text-align: center;
       padding-left: 0rem;
       padding-right: 0rem;
   }
   .col-md-4.media-col4 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 33.333333%;
        flex: 0 0 50%;
        max-width: 50%;
    }
}

/* Large devices (desktops, 992px and up) */
@media (min-width: 992px) and (max-width: 1199px) {    
    .col-md-4.media-col4 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 33.333333%;
        flex: 0 0 50%;
        max-width: 50%;
    }
 }

/* Extra large devices (large desktops, 1200px and up) */
@media (min-width: 1200px) { 
    .media-card{
       padding-top: 0.5rem;
   }
}
</style>